﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameSceneManager : MonoBehaviour
{
    #region Initializations
    public PlayerController player;
    public PlayerController2 player2;
    public Camera mainCamera;
    public Camera camera2;
    public Text player1F;
    public Text player2F;
    public Text player1Win;
    public Text player2Win;
    private int x;
    private int y;
    private int z;
    #endregion Initializations

    #region Methods
    public void Start()
    {
        x = 3;
        y = 1;
        z = 3;
    }

    public void Update()
    {
        // LERP fa una interpolació lineal. Aixo fara un moviment de camara molt més smooth que no si directament ho posem a la posició del jugador
        if (player != null)
        {
            mainCamera.transform.position = new Vector3
            (
                //volem la posició no exactament en el jugador, sino que la volem una mica enrere i damunt seu. Si el jugador gira la camara s'hauria de moure respecte al seu nou darrere, orbitant
                Mathf.Lerp(mainCamera.transform.position.x, (player.transform.position.x - (player.transform.forward.x * x)), Time.deltaTime * 10),
                Mathf.Lerp(mainCamera.transform.position.y, player.transform.position.y + y, Time.deltaTime * 10),
                Mathf.Lerp(mainCamera.transform.position.z, (player.transform.position.z - (player.transform.forward.z * z)), Time.deltaTime * 10)
            );
            //fa una rotació automàtica per a que miri al jugador
            mainCamera.transform.LookAt(player.transform);
        }

        if (player2 != null)
        {
            camera2.transform.position = new Vector3
            (
                //volem la posició no exactament en el jugador, sino que la volem una mica enrere i damunt seu. Si el jugador gira la camara s'hauria de moure respecte al seu nou darrere, orbitant
                Mathf.Lerp(camera2.transform.position.x, (player2.transform.position.x - (player2.transform.forward.x * x)), Time.deltaTime * 10),
                Mathf.Lerp(camera2.transform.position.y, player2.transform.position.y + y, Time.deltaTime * 10),
                Mathf.Lerp(camera2.transform.position.z, (player2.transform.position.z - (player2.transform.forward.z * z)), Time.deltaTime * 10)
            );
            //fa una rotació automàtica per a que miri al jugador
            camera2.transform.LookAt(player2.transform);
        }

        //Cuando muere el jugador 1 se muestra que ha ganado el jugador 2 
        if (player.GetComponent<PlayerController>().Globos <= 0 || player.gameObject == null)
        {
            player.GetComponent<PlayerController>().Morision = true;
            player1F.gameObject.SetActive(true);
            player2Win.gameObject.SetActive(true);
            if (Input.GetKeyDown("r"))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
        //Cuando muere el jugador 2 se muestra que ha ganado el jugador 1 
        if (player2.GetComponent<PlayerController2>().Globos <= 0 || player2.gameObject == null)
        {
            player2.GetComponent<PlayerController2>().Morision = true;
            player2F.gameObject.SetActive(true);
            player1Win.gameObject.SetActive(true);
            if (Input.GetKeyDown("r"))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
        #endregion Methods
    }
}