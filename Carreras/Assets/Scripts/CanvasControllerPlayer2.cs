﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasControllerPlayer2 : MonoBehaviour
{
    #region Initializations
    public PlayerController2 Player;
    public Text TextGlobos;
    public Text TextNumObjDisp;
    public Text TextF;
    public Text TextWin;
    #endregion Initializations

    #region Methods
    void Update()
    {
        //Actualiza los valores
        TextGlobos.text = "Vida actual : " + Player.Globos / 2;
        TextNumObjDisp.text = "x" + Player.numObjetoDisponible;
    }
    #endregion Methods
}
