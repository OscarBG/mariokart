﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    #region Methods
    void Update()
    {
        //Hace que la caja rote
        transform.RotateAround(transform.position, transform.right, Time.deltaTime * 200f);
    }
    #endregion Methods
}
